﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProyectoKnockoutjs.Models
{

    /*
     * Modelo para crear nuestra pequeñisima base de datos xD
     */
    public class Persona
    {
        [Key]
        public int IdPersona { get; set; }
        [Required]
        public String NombrePersona { get; set; }
        [Required]
        public String ApellidoPersona { get; set; }
        [Required]
        public String EdadPersona { get; set; }

    }
}