﻿namespace ProyectoKnockoutjs.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ContextoBase : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'ContextoBase' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'ProyectoKnockoutjs.Models.ContextoBase' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'ContextoBase'  en el archivo de configuración de la aplicación.
        public ContextoBase()
            : base("name=ContextoBase")
        {
        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

        public virtual DbSet<Persona> Personas { get; set; } 

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}