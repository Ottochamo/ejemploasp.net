﻿function ViewModel() {

    var self = this;
    self.agregarPersona = ko.observable(false);
    self.nuevaPersonaEntity = {
        IdPersona: ko.observable(0),
        NombrePersona: ko.observable(''),
        ApellidoPersona: ko.observable(''),
        EdadPersona: ko.observable('')
    };

    const URL = '/api/Personas';

    self.personas = ko.observableArray([]);

    self.getPersonas = function () {
        ajaxHelper('GET', null, URL)
            .then(function (data) {
                self.personas(data);
            });
    };

    self.postPersona = function () {
            
        let dataAEnviar = {
            NombrePersona: self.nuevaPersonaEntity.NombrePersona(), // se ponen los parentesis al final para obtener los valores que tienen en ese momento
            ApellidoPersona: self.nuevaPersonaEntity.ApellidoPersona(),
            EdadPersona: self.nuevaPersonaEntity.EdadPersona()
        };

        console.log(dataAEnviar); // Muestra nuestra informacion ingresada en la consola

        ajaxHelper('POST', JSON.stringify(dataAEnviar), URL)
            .then(function (data) {
                // dejamos los valores en blanco
                limpiarCampos();
                self.getPersonas();
            });
    };

    self.nuevaPersona = function () {
        self.agregarPersona(true);
    };

    self.cancelarNuevaPersona = function () {
        self.agregarPersona(false);
    };

    /**
     * 
     * @param {String} method - Tipo de peticion que vas a hacer
     * @param {JSON} data - Informacion que vas a mandar
     * @param {String} uri - Url a realizar peticion
     */

    function ajaxHelper(method, data, uri) {
        return $.ajax({
            url: uri,
            method: method,
            contentType: "application/json",
            dataType: "json",
            data: data
        });
    }

    function limpiarCampos() {
        self.nuevaPersonaEntity.NombrePersona();
        self.nuevaPersonaEntity.ApellidoPersona();
        self.nuevaPersonaEntity.EdadPersona();
    }

    self.getPersonas();
}

$(document).ready(() => {
    ko.applyBindings(new ViewModel());
});