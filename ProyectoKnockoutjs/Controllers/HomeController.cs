﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoKnockoutjs.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {

            /*
             Este es un controlador de vista, le indica a que vista debe dirigirse el usuario cuando presione un link etc.
             Los nombres de los metodos indican el nombre de la vista a la que hace referencia
             Si te vas a la carpeta Views/Home vas a encontrar un archivo Index.cshtml el cual hace referencia a este metodo

             En si el nombre de la vista sera el nombre de tu metodo

             */

            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
