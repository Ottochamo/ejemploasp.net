namespace ProyectoKnockoutjs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        IdPersona = c.Int(nullable: false, identity: true),
                        NombrePersona = c.String(nullable: false),
                        ApellidoPersona = c.String(nullable: false),
                        EdadPersona = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdPersona);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Personas");
        }
    }
}
